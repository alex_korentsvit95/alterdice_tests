
git clone https://alex_korentsvit95@bitbucket.org/alex_korentsvit95/alterdice_tests.git


$ cd alterdice_tests

$ pip install -r requirements.txt

### Running
To see a list of all tests without running them:
$ python -m pytest -vs --collect-only


To run a single test:
$ python -m pytest --browser=chrome --env=DEV tests/test_orders.py

$ python -m pytest --browser=chrome --env=DEV tests/test_commission_withdrow.py


all necessary parameters in variables.json