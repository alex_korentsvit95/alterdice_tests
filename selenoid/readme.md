
[selenoid docker hub]:https://hub.docker.com/r/aerokube/selenoid/tags/


docker build -t kiwi-selenoid .
# docker run --rm --name selenoid kiwi-selenoid  - It does not works


```sh
docker run --rm -p 4444:4444                        \
    -v /var/run/docker.sock:/var/run/docker.sock    \
    --name selenoid kiwi-selenoid



docker run -d --rm                                  \
    --name selenoid                                 \
    -e TZ=America/Toronto                           \
    -p 4444:4444                                    \
    -v /var/run/docker.sock:/var/run/docker.sock    \
    -v `pwd`/selenoid/:/etc/selenoid/:ro            \
    aerokube/selenoid:1.7.2
```


python -m pytest --lf -n 1 --browser=chrome --maximize_window --remote-browser="http://localhost:4444/wd/hub" --env=DEV tests/




Links:
 1. http://aerokube.com/selenoid/latest/
 1. http://aerokube.com/selenoid-ui/latest/
 1. http://aerokube.com/cm/latest/
 1. https://hub.docker.com/r/aerokube/selenoid/tags/


```sh
docker run -d --rm                                        \
           -v /var/run/docker.sock:/var/run/docker.sock   \
           -e TZ=America/Toronto                          \
           -v ${HOME}:/root                               \
           -e OVERRIDE_HOME=${HOME}                       \
           aerokube/cm:latest-release selenoid start --vnc -e TZ=America/Toronto --tmpfs 128

sleep 3
 ```
