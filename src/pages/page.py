import time

import allure
from selenium.common.exceptions import ElementNotVisibleException, \
    TimeoutException, NoSuchElementException, WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from typing import List

from src.taf.config import BROWSER_IMPLICITLY_WAIT
from src.taf.log_config import logger


class Page(object):
    """
    Base class for all Pages
    """

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.timeout = BROWSER_IMPLICITLY_WAIT
        self.wait = WebDriverWait(self.driver, self.timeout)

    # @pytest.allure.step("Open url '{1}'")
    def open_url(self, url):
        logger.info("Open url '%s'", url)
        self.driver.get(url)
        self.wait_for_page_loaded()

    @property
    def is_page_loaded(self) -> bool:
        page_state = self.driver.execute_script('return document.readyState;')
        return page_state == 'complete'

    def wait_for_page_loaded(self):
        self.wait.until(lambda s: self.is_page_loaded)

    @property
    def url_current_page(self) -> str:
        return self.driver.current_url

    def maximize_window(self):
        try:
            self.driver.maximize_window()
        except WebDriverException:
            pass

    @property
    def page_title(self) -> str:
        self.wait.until(lambda s: s.title)
        return self.driver.title

    @property
    def page_text(self) -> str:
        return self.s((By.CSS_SELECTOR, "body")).text

    def is_element_visible(self, by, value) -> bool:
        try:
            return self.driver.find_element(by, value).is_displayed()
        except (NoSuchElementException, ElementNotVisibleException):
            # this will return a snapshot, which takes time.
            return False

    def is_element_present(self, by, value) -> bool:
        self.driver.implicitly_wait(0)
        try:
            self.driver.find_element(by, value)
            return True
        except (NoSuchElementException, TimeoutException):
            # this will return a snapshot, which takes time.
            return False
        finally:
            # set back to where you once belonged
            self.driver.implicitly_wait(self.timeout)

    def wait_for_element_to_be_visible(self, *locator):
        """Wait for an element to become visible"""
        self.driver.implicitly_wait(0)
        try:
            self.wait.until(lambda s: self.driver.find_element(*locator).is_displayed())
            return self.s(locator)
        finally:
            self.driver.implicitly_wait(self.timeout)

    def wait_for_element_present(self, *locator):
        self.driver.implicitly_wait(0)
        try:
            return self.wait.until(lambda s: self.driver.find_element(*locator))
        finally:
            self.driver.implicitly_wait(self.timeout)

    def wait_for_element_not_present(self, *locator):
        self.driver.implicitly_wait(0)
        try:
            self.wait.until(lambda s: len(self.driver.find_elements(*locator)) < 1)
        finally:
            self.driver.implicitly_wait(self.timeout)

    def wait_for_element_to_be_clickable(self, *locator) -> bool:
        self.driver.implicitly_wait(0)
        try:
            self.wait.until(EC.element_to_be_clickable(*locator))
            # self.s(*locator).is_displayed()
            return True
        except (NoSuchElementException, TimeoutException):
            # this will return a snapshot, which takes time.
            return False
        finally:
            self.driver.implicitly_wait(self.timeout)

    def wait_for_ajax(self):
        self.driver.implicitly_wait(2)
        try:
            self.wait.until(lambda s: s.execute_script('return window.$ != undefined && $.active === 0'))
        finally:
            self.driver.implicitly_wait(self.timeout)

    def attach_page_screenshot(self, description="Page screenshot"):
        page_png = self.driver.get_screenshot_as_png()
        allure.attach(page_png, description, allure.attachment_type.PNG)

    def type_in_element(self, locator, text):
        logger.info("type text '%s'", text)
        """
        Type a string into an element.

        This method clears the element first then types the string via send_keys.

        Arguments:
        locator -- a locator for the element
        text -- the string to type via send_keys
        """
        text_fld = self.s(locator)
        text_fld.clear()
        text_fld.send_keys(text)

    def type_in_element_slowly(self, locator, text):
        logger.info("type text slowly '%s'", text)
        element = self.s(locator)
        element.clear()
        for letter in text:
            element.send_keys(letter)
            time.sleep(.2)

    def type(self, element: WebElement, text: str):
        logger.info("type text '%s'", text)
        element.clear()
        element.send_keys(text)

    def s(self, locator) -> WebElement:
        return self.driver.find_element(*locator)

    def ss(self, locator) -> List[WebElement]:
        return self.driver.find_elements(*locator)

    def go_back(self):
        logger.info("Go back")
        self.driver.back()
        self.wait_for_ajax()

    def refresh(self):
        logger.info("Refresh page")
        self.driver.refresh()
        self.wait_for_ajax()
        return self

    def click_on_element(self, locator, timeout=4, polling=0.2):
        end_time = time.time() + timeout

        while True:
            try:
                self.s(locator).click()
                # self.wait_for_ajax()
                return
            except Exception as reason:
                reason_message = getattr(reason, 'msg', getattr(reason, 'message', getattr(reason, 'args', '')))
                reason_string = '{name}: {message}'.format(name=reason.__class__.__name__, message=reason_message)
                screen = getattr(reason, 'screen', None)
                stacktrace = getattr(reason, 'stacktrace', None)

                if time.time() > end_time:
                    raise TimeoutException('''
                    failed while waiting {timeout} seconds
                    to assert {condition}
                    for {locator}

                    reason: {reason}'''.format(timeout=timeout,
                                               condition="Click",
                                               locator=locator,
                                               reason=reason_string), screen, stacktrace)

                time.sleep(polling)

    def set_language(self):
        self.driver.delete_all_cookies()
        item = 'setting-language'
        value = 'en'
        self.driver.execute_script("window.localStorage.setItem('{0}','{1}');".format(item, value))
        self.refresh()

    def hide_hub_spot_window(self):
        iframe = (By.CSS_SELECTOR, 'iframe')
        window = (By.CSS_SELECTOR, '.initial-message-bubble button')
        logger.info("Close 'Hub Spot modal' window..")
        if self.wait_for_element_to_be_clickable(iframe):
            self.driver.switch_to.frame(self.s(iframe))
            if not self.s((By.CSS_SELECTOR, '.fade-transition-container')).is_displayed():
                self.wait.until(EC.element_to_be_clickable(window))
                self.click_on_element(window)
                logger.info("'Hub Spot' window.. closed")
            else:
                logger.warn("'Hub Spot' window was folded")
            self.driver.switch_to.default_content()
        return self

    def then(self):
        return self

    def select_by_text(self, locator, text: str):
        logger.info("select by visible text '%s'", text)
        select = Select(self.s(locator))
        select.select_by_visible_text(text)

    def select_native_by_text(self, element: WebElement, text: str):
        logger.info("select native by visible text '%s'", text)
        select = Select(element)
        select.select_by_visible_text(text)

    def select_by_value(self, locator, value: str):
        logger.info("select by value '%s'", value)
        select = Select(self.s(locator))
        select.select_by_value(value)

    def select_options(self, locator):
        select = Select(self.s(locator))
        return [e.text for e in select.options]

    def open_new_tab(self):
        logger.info("Open new tab")
        self.driver.execute_script("window.open('about:blank','_blank');")
        self.switch_to_tab(1)

    def switch_to_tab(self, num: int):
        logger.info("Switching to the tab number '%s'", num)
        tabs = self.driver.window_handles
        self.driver.switch_to.window(tabs[num])
        return self

    def scroll_to_element(self, element):
        try:
            self.driver.execute_script("return arguments[0].scrollIntoView();", element)
        except Exception as e:
            logger.error('error scrolling down web element', e)

    def pause(self, value):
        logger.warn("Sleep for '%s' sec", str(value))
        time.sleep(value)
        return self
