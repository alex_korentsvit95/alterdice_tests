import pytest
from time import sleep


from selenium import webdriver
from selenium.webdriver.common.by import By
from src.taf.log_config import logger
from src.pages.page import Page

class TradingPage(Page):


	_login_btn = (By.CLASS_NAME, 'nav-item')
	_email_field = (By.NAME, "email")
	_password_field = (By.NAME, "password")
	_confirm_form = (By.CLASS_NAME, "blue-btn")

	_order_selection = (By.CSS_SELECTOR, ".minimal")
	_volume = (By.NAME, "volume")
	_buy_btn = (By.CSS_SELECTOR, ".mb-1")
	_limit_price = (By.NAME, "rate")
	_no_active_orders = (By.CLASS_NAME, "no-item")

	def open(self, base_url, currency):
		self.driver.get(base_url + '/trading/' + currency)
		return self

	def login(self, email, password):
		self.ss(self._login_btn)[3].click()
		sleep(1)
		self.type_in_element(self._email_field, email)
		self.type_in_element(self._password_field, password)
		self.ss(self._confirm_form)[1].click()
		sleep(2)

	def get_balances(self, сurrency):

		tbody = self.driver.find_elements_by_tag_name('tbody')[0]
		trs = tbody.find_elements_by_tag_name('tr')

		usd = trs[0].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		btc = trs[1].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		eth = trs[2].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		ltc = trs[3].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		zec = trs[4].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		dash = trs[5].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		xrp = trs[6].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		edr = trs[7].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')

		balances_dict = {
			'USD': usd,
			'BTC': btc,
			'ETH': eth,
			'LTC': ltc,
			'ZEC': zec,
			'DASH': dash,
			'XRP': xrp,
			'EDR': edr
		}

		currency1 = сurrency.split('/')[0]
		currency2 = сurrency.split('/')[1]
		currency1_bal = balances_dict[currency1]
		currency2_bal = balances_dict[currency2]


		return currency1_bal, currency2_bal

	def make_market_buy_order(self, currency_1, currency_2, base_url, volume, fee, currency):


		logger.info("Select market order")
		selection = self.ss(self._order_selection)[1]
		selection.click()
		select_market = selection.find_elements_by_tag_name('option')[1]
		select_market.click()
		logger.info("Typing volume: " + volume)
		self.type_in_element(self._volume, volume)


		logger.info("Press Exchange buy and check balance")
		self.ss(self._buy_btn)[4].find_elements_by_tag_name('button')[0].click()
		sleep(2)
		self.driver.get(base_url + '/trading/' + currency)
		sleep(2)

		logger.info("Checking status of the last order")
		order_status = self.driver.find_elements_by_class_name('table')[2].find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[8].find_element_by_tag_name('div').text.strip()
		logger.info("Status: " + order_status)

		price = self.driver.find_elements_by_class_name('table')[2].find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[6].find_element_by_tag_name('div').get_attribute('innerHTML').split('data-original-title="')[1].split('">')[0].replace(',', '')
		logger.info("Price: " + price)

		expected_currency_2 = str(float(currency_2) - float(price))
		logger.info("Expected currency 2 balance: " + expected_currency_2)

		expected_currency_1 = str((float(currency_1) + float(volume)) - float(volume)/100 * float(fee))
		logger.info("Expected currency 1 balance: " + expected_currency_1)

		assert order_status == 'DONE'


		return expected_currency_1, expected_currency_2


	def make_market_sell_order(self, currency_1, currency_2, base_url, volume, fee, currency):

		logger.info("Select market order")
		selection = self.ss(self._order_selection)[1]
		selection.click()
		select_market = selection.find_elements_by_tag_name('option')[1]
		select_market.click()

		logger.info("Typing volume: " + volume)
		self.type_in_element(self._volume, volume)


		logger.info("Press Exchange sell and check balance")
		self.ss(self._buy_btn)[4].find_elements_by_tag_name('button')[1].click()
		sleep(2)
		self.driver.get(base_url + '/trading/' + currency)
		sleep(2)

		logger.info("Checking status of the last order")
		order_status = self.driver.find_elements_by_class_name('table')[2].find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[8].find_element_by_tag_name('div').text.strip()
		logger.info("Status: " + order_status)

		price = self.driver.find_elements_by_class_name('table')[2].find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[6].find_element_by_tag_name('div').get_attribute('innerHTML').split('data-original-title="')[1].split('">')[0].replace(',', '')
		logger.info("Price: " + price)

		expected_currency_2 = str((float(currency_2) + float(price)) - float(price)/100 * float(fee))
		logger.info("Expected currency 2 balance: " + expected_currency_2)

		expected_currency_1 = str(float(currency_1) - float(volume))
		logger.info("Expected currency 1 balance: " + expected_currency_1)

		assert order_status == 'DONE'

		return expected_currency_1, expected_currency_2



	def make_limit_buy_order(self, currency_1, currency_2, base_url, volume, fee, currency):


		logger.info("currency 1 Balance: " + currency_1)

		logger.info("Typing volume: " + volume)
		self.type_in_element(self._volume, volume)

		logger.info("Getting last price and type it into price field")
		last_price = self.driver.find_elements_by_class_name("table-transform")[3].find_element_by_tag_name("tbody").find_elements_by_tag_name("tr")[5].find_elements_by_tag_name("td")[0].text.replace(',', '')
		logger.info("Last Price: " + last_price)
		#last_price = str(float(last_price) + 0.02)
		self.type_in_element(self._limit_price, last_price)


		logger.info("Press Exchange buy and check balance")
		self.ss(self._buy_btn)[4].find_elements_by_tag_name('button')[0].click()
		sleep(2)

		while True:
			try:
				active_order = self.s(self._no_active_orders)
				sleep(2)
				break
			except:
				sleep(3)

		self.driver.get(base_url + '/trading/' + currency)
		sleep(2)
	

		logger.info("Checking status of the last order")
		order_status = self.driver.find_elements_by_class_name('table')[2].find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[8].find_element_by_tag_name('div').text.strip()
		logger.info("Status: " + order_status)

		price = self.driver.find_elements_by_class_name('table')[2].find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[6].find_element_by_tag_name('div').get_attribute('innerHTML').split('data-original-title="')[1].split('">')[0].replace(',', '')
		logger.info("Price: " + price)

		expected_currency_2 = str(float(currency_2) - float(price))
		logger.info("Expected currency 2 balance: " + expected_currency_2)

		expected_currency_1 = str((float(currency_1) + float(volume)) - float(volume)/100 * float(fee))
		logger.info("Expected currency 1 balance: " + expected_currency_1)

		assert order_status == 'DONE'

		return expected_currency_1, expected_currency_2


	def make_limit_sell_order(self, currency_1, currency_2, base_url, volume, fee, currency):


		logger.info("Typing volume: " + volume)
		self.type_in_element(self._volume, volume)

		logger.info("Getting last price and type it into price field")
		last_price = self.driver.find_elements_by_class_name("table-transform")[1].find_element_by_tag_name("tbody").find_elements_by_tag_name("tr")[5].find_elements_by_tag_name("td")[-1].text.replace(',', '')
		logger.info("Last Price: " + last_price)
		#last_price = str(float(last_price) - 0.02)
		self.type_in_element(self._limit_price, last_price)


		logger.info("Press Exchange sell and check balance")
		self.ss(self._buy_btn)[4].find_elements_by_tag_name('button')[1].click()
		sleep(2)

		while True:
			try:
				active_order = self.s(self._no_active_orders)
				sleep(2)
				break
			except:
				sleep(3)


		self.driver.get(base_url + '/trading/' + currency)
		sleep(2)

		logger.info("Checking status of the last order")
		order_status = self.driver.find_elements_by_class_name('table')[2].find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[8].find_element_by_tag_name('div').text.strip()
		logger.info("Status: " + order_status)

		price = self.driver.find_elements_by_class_name('table')[2].find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[6].find_element_by_tag_name('div').get_attribute('innerHTML').split('data-original-title="')[1].split('">')[0].replace(',', '')
		logger.info("Price: " + price)

		expected_currency_2 = str((float(currency_2) + float(price)) - float(price)/100 * float(fee))
		logger.info("Expected currency 2 balance: " + expected_currency_2)

		expected_currency_1 = str(float(currency_1) - float(volume))
		logger.info("Expected currency 1 balance: " + expected_currency_1)

		assert order_status == 'DONE'

		return expected_currency_1, expected_currency_2