import pytest
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.by import By
from src.taf.log_config import logger
from src.pages.page import Page

class Withdrow(Page):

	_form = (By.CLASS_NAME, 'transparent-field')
	_commission = (By.CLASS_NAME, 'hover-point')

	_address_field = (By.NAME, 'crypto_address')
	_amount = (By.NAME, 'amount')
	_check_box = (By.NAME, 'user_agree')
	_submit_btn = (By.CLASS_NAME, 'white-btn')
	_verify_code = (By.NAME, 'verify_code')

	# For USD
	_select_country = (By.NAME, 'country')
	_select_currency = (By.NAME, 'country_money')
	_pay_system = (By.NAME, 'pay_system')
	_sum = (By.NAME, 'sum')
	_requisites = (By.NAME, 'requisites')
	_close_modal = (By.CLASS_NAME, 'btn-close')


	def open(self, base_url):

		withdrow_page = self.driver.get(base_url + '/withdraw')
		sleep(2)
		return withdrow_page

	
	def get_balances(self):

		tbody = self.driver.find_elements_by_tag_name('tbody')[0]
		trs = tbody.find_elements_by_tag_name('tr')

		usd = trs[0].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		btc = trs[1].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		eth = trs[2].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		ltc = trs[3].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		zec = trs[4].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		dash = trs[5].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')
		#edr = trs[6].find_elements_by_tag_name('td')[3].find_element_by_tag_name('span').get_attribute('data-original-title')

		balances_dict = {
			#'USD': usd,
			'BTC': btc,
			'ETH': eth,
			'LTC': ltc,
			'ZEC': zec,
			'DASH': dash,
			#'EDR': edr
		}

		return balances_dict


	def make_withdrow(self):

		commission_list = []

		# USD WITHDROW
		# button = self.driver.find_elements_by_class_name(' mb-3')[-1].find_elements_by_class_name('hover-point')[-1]
		# button.click()
		# sleep(2)

		# select_country = self.s(self._select_country)
		# select_country.click()
		# select_country.find_elements_by_tag_name('option')[1].click()
		# sleep(1)

		# select_currency = self.s(self._select_currency)
		# select_currency.click()
		# select_currency.find_elements_by_tag_name('option')[1].click()
		# sleep(1)

		# pay_system = self.s(self._pay_system)
		# pay_system.click()
		# pay_system.find_elements_by_tag_name('option')[2].click()
		# sleep(1)

		# self.type_in_element(self._sum, '10')

		# commission = self.ss(self._form)[1].find_element_by_class_name("hover-point").text.strip()#.get_attribute('data-original-title')
		# logger.info('Commission: ' + commission)
		# commission = str(float(commission) + 10)
		# commission_list.append(commission)

		# self.type_in_element(self._requisites, 'Test')
		# self.s(self._submit_btn).click()
		# #sleep(8)
		# #self.s(self._close_modal).click()
		# sleep(8)


		#buttons = self.driver.find_elements_by_class_name(' mb-3')[-1].find_elements_by_class_name('hover-point')[:-1]

		#for i in range(len(buttons)):
		for i in range(5):

			amount = 1

			button = self.driver.find_elements_by_class_name(' mb-3')[-1].find_elements_by_class_name('hover-point')[i]
			button.click()
			sleep(2)
			self.type_in_element(self._address_field, 'teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest')
			self.type_in_element(self._amount, str(amount))
			
			commission = self.ss(self._form)[1].find_element_by_class_name("hover-point").text.strip().split(' ')[0]#.get_attribute('data-original-title')
			logger.info('Commission: ' + commission)
			commission = str(float(commission) + amount)
			commission_list.append(commission)

			self.ss(self._form)[1].find_element_by_class_name('font-12').click()
			self.s(self._submit_btn).click()
			sleep(3)
			
			while True:

				try:
					enter_code_text = self.ss(self._form)[1].find_elements_by_tag_name('p')[1].text
				except:
					enter_code_text = ''

				if 'Enter code to proceed' in enter_code_text:
					sleep(3)				
				else:
					break

			sleep(2)
			self.s(self._submit_btn).click()
			sleep(5)

		return commission_list

