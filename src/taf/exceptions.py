# -*- coding: utf-8 -*-

"""
taf.exceptions
~~~~~~~~~~~~~~~~~~~

This module contains the set of Frameworks' exceptions.

"""


class FrameworkException(RuntimeError):
    """There was an ambiguous exception that occurred while handling your request."""


class LoginError(FrameworkException):
    """A Login error occurred."""


class TestError(FrameworkException):
    """A Test error occurred."""


class ApiError(FrameworkException):
    """An Api error occurred."""


class ConfigurationError(FrameworkException):
    """A Configuration error occurred."""
