# -*- coding: utf-8 -*-

"""
gmail.exceptions
~~~~~~~~~~~~~~~~~~~

This module contains the set of Gmails' exceptions.

"""


class EmailException(RuntimeError):
    """There was an ambiguous exception that occurred while handling your
    request."""


class ConnectionError(EmailException):
    """A Connection error occurred."""


class AuthenticationError(EmailException):
    """Gmail Authentication failed."""


class Timeout(EmailException):
    """The request timed out."""
