"""

GMail! Woo!

"""

__title__ = 'gmail'
__version__ = '0.1'
__author__ = 'Charlie Guo'
__build__ = 0x0001
__license__ = 'Apache 2.0'
__copyright__ = 'Copyright 2013 Charlie Guo'

from .exceptions import EmailException, ConnectionError, AuthenticationError  # noqa: F401
from .gmail import Gmail  # noqa: F401
from .mailbox import Mailbox  # noqa: F401
from .message import Message  # noqa: F401
from .utils import login, authenticate  # noqa: F401
