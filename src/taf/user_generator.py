import json
import random
from faker import Faker

from src.taf import currency, category
from src.taf.utils import get_random_string


#
# def generate_prof():
#     user_type = random.choice(user_types)
#     salutation = random.choice(salutations)
#     first_name = random.choice(first_names_male)
#     last_name = random.choice(last_names) + ' Test'
#     tmp = get_variables()['users']['default']['email'].split('@')
#     password = get_random_string().lower()
#     new_email = "{0}+{1}@{2}".format(tmp[0], password, tmp[1]).lower()
#     if salutation == "Mrs":
#         first_name = random.choice(first_names_female)
#     return User(user_type, salutation, first_name, last_name, new_email, password, offline)


# def generate_offline_user():
#     return generate_new_user(offline=True)
#
#
# def generate_online_user():
#     return generate_new_user(offline=False)


def get_variables():
    with open('variables.json', encoding='utf-8') as data_file:
        data = json.loads(data_file.read())
        return data


def user_type_to_segment_id(user_type):
    map = {'Resident': 15, 'Employee': 35, 'Service provider': 40}
    return map.get(user_type)


class Service(object):
    """DTO"""

    def __init__(self):
        self.name = ''
        self.description = ''
        self.duration = ''
        self.price = ''
        self.tax = ''
        self.currency = ''

    def __repr__(self) -> str:
        return "Service: {}".format(self.name)

    def generate(self):
        fake = Faker()
        service = Service()
        service.name = fake.color_name() + " " + fake.cryptocurrency_name()
        service.description = fake.text()
        service.duration = random.randint(5, 120)
        service.price = random.randint(0, 100000)
        service.tax = random.randint(0, 55)
        service.currency = random.choice(currency)
        return service


class Address(object):
    """DTO"""

    def __init__(self):
        self.city = ''
        self.country = ''

    def generate(self):
        fake = Faker('de_DE')
        address = Address()
        address.city = fake.city()
        address.country = "Germany"
        return address

    def __repr__(self) -> str:
        return "Address: {}, {}".format(self.city, self.country)


class WorkingHours(object):
    """DTO"""

    def __init__(self):
        pass


class User(object):
    """"Test DTO for creating new account"""

    def __init__(self):
        self.username = ''
        self.first_name = ''
        self.last_name = ''
        self.email = ''
        self.password = ''
        self.comment = ''
        self.phone = ''

    def generate(self):
        fake = Faker()
        user = User()
        user.username = fake.user_name()
        name = fake.name().split(" ")
        user.first_name = name[0]
        user.last_name = name[1]
        user.password = get_random_string().lower()
        tmp = get_variables()['users']['gmail_user']['email'].split('@')
        user.email = "{0}+{1}@{2}".format(tmp[0], user.password, tmp[1]).lower()
        user.comment = fake.sentence()
        user.phone = fake.phone_number().replace("x", "")
        return user

    def __repr__(self) -> str:
        return "User: {}, {}, {}, {}, {}".format(self.username, self.first_name, self.last_name,
                                                 self.email, self.password)

    def __str__(self) -> str:
        return super().__str__()


class BusinessInfo(object):
    """DTO"""

    def __init__(self):
        self.category = ''
        self.business_name = ''
        self.desc = ''
        self.address = ''
        self.phone = ''
        self.services = []

    def __repr__(self) -> str:
        return "BusinessInfo: {}, {}".format(self.category, self.business_name)

    # https://github.com/deepthawtz/faker

    def generate(self):
        # fake = Faker()
        fake = Faker('de_DE')
        buss = BusinessInfo()
        buss.category = random.choice(category)
        buss.business_name = fake.company()
        buss.desc = ". ".join([fake.sentence() for _ in range(5)])

        # buss.address = fake.address().replace("\n", ", ")
        buss.address = Address().generate()
        buss.phone = fake.phone_number()
        for _ in range(random.randint(2, 7)):
            buss.services.append(Service.generate(self))
        # buss.service = Service.generate(self)

        return buss
