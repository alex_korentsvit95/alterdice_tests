import time

import copy
from lxml import html

from src.taf.gmail import EmailException
from src.taf.gmail import Gmail
from src.taf.log_config import logger


def find_email_by_recipient(existing_user, recipient):
    return find_email_by(existing_user, to=recipient)


def find_email_by(gmail_user, pooling_period=5, **kwargs):
    logger.info("login to email box '%s'..", gmail_user['email'])
    logger.info("reading emails..")
    count = 0
    box = None
    messages = None
    tries = 11
    # wait before reading emails
    time.sleep(3)
    while count <= tries:
        box = Gmail()
        box.login(gmail_user['email'], gmail_user['pass'])
        messages = box.inbox().mail(**kwargs)
        logger.info("\tGot '%s' emails in the box", len(messages))
        if len(messages) > 0:
            break
        logger.info("\t\twaiting for '%s' sec..", pooling_period)
        box.logout()
        time.sleep(pooling_period)
        count += 1
    if len(messages) == 0:
        raise EmailException("No message for '{}' after '{}' sec.".format(str(kwargs), pooling_period * tries))

    for message in messages:
        message.fetch()
        message.read()
        result = copy.copy(message)
        logger.info("\tDelete email with subject '%s'..", message.subject)
        message.delete()
        box.logout()
        logger.info("email logout..")
        return result


def get_reset_password_link(message):
    return search_by_href_contains(message, '#pwreset')


def get_change_email_link(message):
    return search_by_href_contains(message, '#changeEmail')


def get_activate_link(message):
    return search_by_href_contains(message, 'verification')


def get_booking_confirmation_link(message):
    return search_by_href_contains(message, 'booking-confirmation')


def get_accept_invitation_link(message):
    return search_by_href_contains(message, '#accept-invitation')


def search_by_href_contains(message, key_word):
    html_message = ""
    if isinstance(message.html, bytes):
        logger.info("looking for a '%s' link..", key_word)
        html_message = message.html.decode('utf8').strip()
        root = html.fromstring(html_message)
        href = root.xpath("//*[contains(@href, '{}')]".format(key_word))[0]
        logger.info("\tlink '%s' is '%s'", key_word, href.get('href'))
        return href.get('href'), html_message
    else:
        raise RuntimeError("no html body in the mail message", html_message)


def clean_inbox(email, password):
    g = Gmail()
    logger.info("login to email box '%s'..", email)
    g.login(email, password)
    logger.info("reading email..")
    messages = g.inbox().mail()
    logger.info("\tgoing to delete '%s' emails..", len(messages))
    for message in messages:
        message.fetch()
        logger.info("\tdelete email with subject '%s'..", message.subject)
        message.read()
        message.delete()
    g.logout()
    logger.info("email logout..")
