import os

env = {
    'DEV': {
        'app': 'https://view-stock.intary.net',
        'api': 'https://view-stock.intary.net'
    },
    'PROD': {
        'app': os.environ.get('APP_URL', 'https://alterdice.com'),
        'api': os.environ.get('API_URL', 'https://alterdice.com'),
    }
}

BROWSER_IMPLICITLY_WAIT = 15
