from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

from src.taf.browser.browsers import BrowserName
from src.taf.chrome_preferences import CHROME_PREFERENCES
from src.taf.config import BROWSER_IMPLICITLY_WAIT
from src.taf.exceptions import FrameworkException
from src.taf.log_config import logger


def get_browser(browser_name) -> WebDriver:
    logger.info("Start '{0}' browser..".format(browser_name))

    # path = os.path.join(os.getcwd(), "src/resources/drivers/linux64x/")
    # service_log_path = os.path.join(os.getcwd(), 'target/logs/{}_{}_{}_driver.log'
    #                                 .format(strftime("%H-%M-%S", localtime()), str(request.node.name), browser_name))

    if browser_name.lower() == BrowserName.FIREFOX:
        capabilities = {'marionette': True}
        _driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(),
                                    capabilities=capabilities)
        # log_path=service_log_path)

    elif browser_name.lower() == BrowserName.CHROME:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--test-type")
        chrome_options.add_argument("--disable-setuid-sandbox")
        chrome_options.add_argument("--disable-infobars")
        chrome_options.add_experimental_option('prefs', CHROME_PREFERENCES)

        # _driver = webdriver.Chrome(executable_path=os.path.join(path,  'chromedriver'),
        _driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(),
                                   chrome_options=chrome_options)
        # service_args=['--verbose'],
        # service_log_path=service_log_path)

    else:
        raise FrameworkException("Could not recognize browser name '{}'".format(browser_name))

    _driver.implicitly_wait(BROWSER_IMPLICITLY_WAIT)

    return _driver
