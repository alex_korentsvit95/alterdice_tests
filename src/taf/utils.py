import locale
import os
import random
import string

from src.taf.log_config import logger


def get_random_string(N=10, extra=''):
    return ''.join(random.choice(string.ascii_letters + string.digits + extra) for _ in range(N))


def get_random_string_german(N=10):
    return get_random_string(N, extra='äüöß')


def sort_list(list_original, reverse=False):
    locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')
    return sorted(list_original, key=locale.strxfrm, reverse=reverse)


def message(web_page_list, sorted_list):
    return "The sequence of elements in lists are mismatched!" \
           "\nExpected: <{0}> \nbut: was <{1}>".format(sorted_list, web_page_list)


def env(key, default=None):
    try:
        return os.environ.get(key, default)
    except KeyError:
        return None


def __get_full_path(file_path=os.path.join(os.getcwd(), "target")):
    for file in os.listdir(file_path):
        if file.endswith(".csv"):
            full_path = os.path.join(file_path, file)
            logger.info("Full path to report '%s'", full_path)
            return full_path


def read_downloaded_csv_report():
    full_path = __get_full_path()
    with open(full_path, "r", encoding="utf-8") as f:
        data = f.read()
        print("\n\n" + data + "\n\n")
        return data


def validate_user_in_report(user):
    file_path = __get_full_path()
    with open(file_path, "r", encoding="utf-8") as f:
        text = "\n".join(f.readlines())
        assert user["customer_number"] in text
        assert user["email"] in text
