import os

CHROME_PREFERENCES = {
    'credentials_enable_service': False,
    "download": {
        "directory_upgrade": True,
        "default_directory": os.path.join(os.getcwd(), 'target')
    },
    'profile': {
        'password_manager_enabled': False
    }
}
