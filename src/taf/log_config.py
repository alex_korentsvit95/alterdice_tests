import logging

logger = logging.getLogger()
handler = logging.StreamHandler()

fh = logging.FileHandler('Test_logs.log')
fh.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s process-id:%(process)d [%(filename)s:%(lineno)d] %(levelname)s: %(message)s')

handler.setFormatter(formatter)
fh.setFormatter(formatter)

logger.addHandler(handler)
logger.addHandler(fh)
logger.setLevel(logging.INFO)
logging.captureWarnings(True)

logging.getLogger("urllib3").setLevel(logging.INFO)

# import http.client as http_client
# requests_log = logging.getLogger("requests.packages.urllib3")
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True
# http_client.HTTPConnection.debuglevel = 1

selenium_logger = logging.getLogger('selenium.webdriver.remote.remote_connection')
# Only display possible problems
selenium_logger.setLevel(logging.WARNING)
