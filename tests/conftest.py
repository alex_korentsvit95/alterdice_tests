import pytest

from src.taf.user_generator import User, BusinessInfo


@pytest.fixture
def new_user():
    return User().generate()


@pytest.fixture
def new_business():
    return BusinessInfo().generate()
