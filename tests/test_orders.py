import allure
import pytest
from time import sleep

from src.pages.trading_page import TradingPage
from src.taf.log_config import logger
from src.taf.email_helper import find_email_by_recipient, get_booking_confirmation_link


@pytest.allure.feature('Test orders')
class  TestOrders:


	@pytest.mark.first
	@pytest.allure.story('Make sure that there is money at the balance')
	def test_make_orders(self, base_url, driver, gmail_user, сurrency, volume, fee):

		logger.info('\n<--------------------------------------------------------------New TEST-------------------------------------------------------------->')
		logger.info('======================================================================================================================================')
		logger.info('CURRENCY: ' + сurrency)
		logger.info('Log in')
		driver.get(base_url)
		TradingPage(driver).login(gmail_user['email'], gmail_user['pass'])

		while True:

			logger.info('Waiting for login')
			log_out_btn = driver.find_elements_by_class_name('nav-link')[-1]

			if 'Logout' in log_out_btn.text:
				logger.info('Login Success')
				sleep(2)
				break
			sleep(3)


		logger.info('Open Trading page')
		trading_page = TradingPage(driver).open(base_url, сurrency.replace('/', ''))
		sleep(3)

		
		logger.info('Gettin current balance')
		currency1_bal, currency2_bal = TradingPage(driver).get_balances(сurrency)


		# MARKET BUY ORDER
		logger.info('Makeing market buy order on the ' + сurrency)
		fee_market = fee['market']
		expected_currency_1, expected_currency_2 = TradingPage(driver).make_market_buy_order(currency1_bal, currency2_bal, base_url, volume, fee_market, сurrency.replace('/', ''))

		logger.info('Gettin current balance')
		currency1_bal, currency2_bal = TradingPage(driver).get_balances(сurrency)
		logger.info('Currency 1 new balance: ' + currency1_bal)
		logger.info('Currency 2 new balance: ' + currency2_bal)

		assert abs(float(currency2_bal) - float(expected_currency_2)) <= 0.0000001
		assert abs(float(currency1_bal) - float(expected_currency_1)) <= 0.0000001


		# MARKET BUY ORDER
		logger.info('Makeing market sell order on the ' + сurrency)
		expected_currency_1, expected_currency_2 = TradingPage(driver).make_market_sell_order(currency1_bal, currency2_bal, base_url, volume, fee_market, сurrency.replace('/', ''))
		sleep(3)

		logger.info('Gettin current balance')
		currency1_bal, currency2_bal = TradingPage(driver).get_balances(сurrency)
		logger.info('Currency 1 new balance: ' + currency1_bal)
		logger.info('Currency 2 new balance: ' + currency2_bal)

		assert abs(float(currency2_bal) - float(expected_currency_2)) <= 0.0000001
		assert abs(float(currency1_bal) - float(expected_currency_1)) <= 0.0000001


		# MARKET BUY ORDER
		logger.info('Makeing limit buy order on the ' + сurrency)
		fee_limit = fee['limit']
		expected_currency_1, expected_currency_2 = TradingPage(driver).make_limit_buy_order(currency1_bal, currency2_bal, base_url, volume, fee_limit, сurrency.replace('/', ''))

		logger.info('Gettin current balance')
		currency1_bal, currency2_bal = TradingPage(driver).get_balances(сurrency)
		logger.info('Currency 1 new balance: ' + currency1_bal)
		logger.info('Currency 2 new balance: ' + currency2_bal)

		assert abs(float(currency2_bal) - float(expected_currency_2)) <= 0.0000001
		assert abs(float(currency1_bal) - float(expected_currency_1)) <= 0.0000001


		# MARKET BUY ORDER
		logger.info('Makeing limit sell order on the ' + сurrency)
		expected_currency_1, expected_currency_2 = TradingPage(driver).make_limit_sell_order(currency1_bal, currency2_bal, base_url, volume, fee_limit, сurrency.replace('/', ''))

		currency1_bal, currency2_bal = TradingPage(driver).get_balances(сurrency)
		logger.info('Currency 1 new balance: ' + currency1_bal)
		logger.info('Currency 2 new balance: ' + currency2_bal)

		assert abs(float(currency2_bal) - float(expected_currency_2)) <= 0.0000001
		assert abs(float(currency1_bal) - float(expected_currency_1)) <= 0.0000001
