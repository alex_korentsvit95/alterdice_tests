import allure
import pytest
from time import sleep

from src.taf.log_config import logger
from src.pages.trading_page import TradingPage
from src.pages.withdrow_page import Withdrow


@pytest.allure.feature('Сhecking commission on withdrow')
class  TestСommissionWithdrow:

	@pytest.mark.first
	@pytest.allure.story('Make withdrow')
	def test_make_withdrow(self, base_url, driver, gmail_user):
		
		logger.info('\n<--------------------------------------------------------------New TEST-------------------------------------------------------------->')
		logger.info('======================================================================================================================================')
		logger.info('Log in')
		driver.get(base_url)
		TradingPage(driver).login(gmail_user['email'], gmail_user['pass'])


		while True:

			logger.info('Waiting for login')
			log_out_btn = driver.find_elements_by_class_name('nav-link')[-1]

			if 'Logout' in log_out_btn.text:
				logger.info('Login Success')
				sleep(2)
				break
			sleep(3)


		logger.info('Open Trading page')
		trading_page = TradingPage(driver).open(base_url, '')
		sleep(3)

		logger.info('Gettin current balances')
		balances_dict = Withdrow(driver).get_balances()
		logger.info('Initial balances')
		logger.info(balances_dict)

		withdrow_page = Withdrow(driver).open(base_url)

		commission_list = Withdrow(driver).make_withdrow()

		expected_balances_dict = {}

		for i, key in enumerate(balances_dict.keys()):
			logger.info(key)
			expected_balances_dict[key] = float(balances_dict[key]) - float(commission_list[i])

		logger.info('Expected balances')
		logger.info(expected_balances_dict)


		logger.info('Open Trading page')
		trading_page = TradingPage(driver).open(base_url, '')
		sleep(3)

		new_balances_dict = Withdrow(driver).get_balances()
		logger.info('Final balances')
		logger.info(new_balances_dict)

		logger.info('Checking commission at every currence')
		for key in new_balances_dict.keys():

			logger.info('Currency: ' + key)
			new_balance = new_balances_dict[key]
			expected_balance = expected_balances_dict[key]

			if key != 'USD':

				assert abs(float(new_balance) - expected_balance) <= 0.00001
				logger.info('Done')


