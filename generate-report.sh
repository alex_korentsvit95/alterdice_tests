#!/usr/bin/env bash


ver=2.7.0
zip_archive=target/allure.zip


if [ ! -f $zip_archive ]; then
   echo "Allure $zip_archive does not exists."
   wget $URL -O $zip_archive
   unzip -o $zip_archive -d target/
fi


#if ! which allures > /dev/null; then
#   echo -e "Command not found! Read description here https://docs.qameta.io/allure/latest/#_commandline"
#fi


allure_results=target/allure-results
allure_report=target/allure-report


if [ "$(ls -A $allure_results)" ]; then
    echo 'remove previous generated report..'
    rm -rf $allure_report

    echo 'generating new report..'
#    cp allure.properties $allure_results
    target/allure-$ver/bin/allure generate $allure_results -o $allure_report
    echo 'done, check how it looks'
else
    echo "folder '$allure_results' does not exist or empty!"
fi
