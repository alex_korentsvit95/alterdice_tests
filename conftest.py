import allure
import pytest
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.remote.webdriver import WebDriver

from src.taf.browser.browser import get_browser
from src.taf.exceptions import FrameworkException
from src.taf.log_config import logger


def pytest_addoption(parser):
    parser.addoption("--browser", action="store", default="chrome", help="Available names [chrome|firefox]")
    parser.addoption("--remote-browser", action="store", help="Remote selenium browser instance")
    parser.addoption("--env", action="store", default="DEV", help="Available names [DEV|PROD]")
    parser.addoption("--maximize_window", action="store_true", required=False, help="Maximize browser window")


@pytest.yield_fixture(scope="function")
def driver(request) -> WebDriver:
    browser = request.config.getoption('--browser')
    remote_browser = request.config.getoption('--remote-browser')
    if remote_browser:
        logger.info("Start Remote browser on host '%s'..", remote_browser)
        capabilities = {"browserName": browser, "enableVNC": True, "enableVideo": False}

        logger.info("Remote Host %s", remote_browser)
        logger.info("Capabilities %s", capabilities)

        _driver = webdriver.Remote(command_executor=remote_browser, desired_capabilities=capabilities)
    else:
        logger.info("Start local browser..")
        _driver = get_browser(browser)

    if request.config.option.maximize_window:
        try:
            _driver.maximize_window()
        except WebDriverException as e:
            _driver.set_window_size(1280, 1024)

    request.cls.driver = _driver
    yield _driver
    print('\n')
    logger.info("teardown the browser..")
    request.cls.driver = None
    _driver.quit()


@pytest.fixture
def environment(request):
    return request.config.getoption("--env").upper()


@pytest.fixture(scope='function')
def base_url(environment, project_name='app'):
    from src.taf.config import env
    try:
        return env.get(environment).get(project_name)
    except AttributeError as err:
        raise FrameworkException("No such environment '{}'".format(environment))


def pytest_exception_interact(node, call, report):
    if hasattr(node, 'cls') and hasattr(node.cls, 'driver') and node.cls.driver is not None:
        logger.error("Taking screenshot on test error..")
        driver = node.cls.driver
        allure.attach(driver.get_screenshot_as_png(), 'Screenshot', allure.attachment_type.PNG)
        allure.attach(driver.page_source, 'Page source', allure.attachment_type.HTML)
        allure.attach(driver.current_url, 'Current url', allure.attachment_type.URI_LIST)


@pytest.fixture
def stored_users(variables):
    return variables['users']


@pytest.fixture
def client_user(stored_users):
    return stored_users['client']


@pytest.fixture
def prof_user(stored_users):
    return stored_users['professional']


@pytest.fixture
def gmail_user(stored_users):
    return stored_users['gmail_user']


@pytest.fixture
def сurrency(stored_users):
    return stored_users['currency']

@pytest.fixture
def volume(stored_users):
    return stored_users['volume']

@pytest.fixture
def fee(stored_users):
    return stored_users['fee']


@pytest.fixture(scope='function')
def login(base_url, driver, prof_user):
    from src.pages.login_page import LoginPage
    login = LoginPage(driver, base_url).open()
    login.login_with(prof_user['login'], prof_user['password'])
